module.exports = (req, res, next) => {
    res.onSuccess = function(result, message) {
        if(message == '') {
            this.send({response : result});
        } else {
            this.send({ response : result, message:message});
        }
    }

    res.onError = function(error) {
        this.send( { message : error, response : {}});
    }
    
    next();
}